#!/bin/bash


# Unmount all sticks
echo "Unmounting all USB drives plugged into hub ..."
for name in $(udisksctl dump | grep by-uuid | grep Id: | grep -P -o '....-....$') ; do
    dev=$(findfs UUID=$name)
    udisksctl unmount -b $dev
done
