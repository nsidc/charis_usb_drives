#!/bin/bash

if [ "$1" != "$USER" ] ; then
    echo "Usage:  $0 <your_user_name>"
    echo "This is for safety."
    exit 1
fi

./mount_all.sh

destmounts_dir='/media/braup'
dests=$(ls $destmounts_dir)
for d in $dests ; do
    dest="$destmounts_dir/$d"
    mkdir -p $dest
    echo "Wiping data from $dest"
    rm -r $dest/*
done

