#!/bin/bash

# mount all disks plugged into the Anker hub
echo "Mounting all USB drives plugged into hub ..."
for name in $(udisksctl dump | grep by-uuid | grep Id: | grep -P -o '....-....$') ; do
    dev=$(findfs UUID=$name)
    udisksctl mount -b $dev
done
echo "Drives mounted"
