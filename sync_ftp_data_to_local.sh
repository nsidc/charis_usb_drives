#!/bin/bash

ftpdir='snow.colorado.edu:/disks/sidads_ftp/pub/projects/CHARIS/2016_Almaty'
localdir="$HOME/work/Projects/CHARIS/Training_workshops/Almaty_2016/FTP_site_copy"

rsync --recursive --update --verbose --delete --modify-window=4 \
      --exclude '*~' --exclude '.git*' --exclude '.fuse*'      \
      --delete-excluded                                         \
      "$ftpdir/" "$localdir/"
