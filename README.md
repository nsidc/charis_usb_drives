# CHARIS USB drive scripts

These are scripts to sync FTP site data to a local copy, and the local copy to
USB flash drives.

## Usage

Check out the project, edit the paths as necessary, and run the scripts.  These
were written to work on a Linux (Ubuntu) system, and have not been tested on
anything else.  The probably won't work on other OSes.

## Contact

Bruce Raup
braup@nsidc.org
