#!/bin/bash

usage=$'Usage:  $0 <chunk>

where <chunk> is one of:  large  small

Use the 64 GB drives for "large"; the 16 GB drives for "small".
The "large" option syncs all the data on the whole FTP site, while the "small"
option syncs materials for the GIS training, and all non-data material for the
main training.'

localdirbase="$HOME/work/Projects/CHARIS/Training_workshops/Almaty_2016/FTP_site_copy"

do_sync() {
    if [ "$2" = "" ] ; then
        echo "do_sync needs two arguments: a directory under the FTP site to sync and a USB stick size (e.g. 15G)"
        exit 1
    fi

    subdir=$1
    req_size=$2

    # The exclude option to this function isn't currently used, but might be useful later.
    if [ "$3" != "" ] ; then
        excl="--exclude '*~' --exclude '.git*' --exclude '.fuse*' --exclude $2"
    else
        excl="--exclude '*~' --exclude '.git*' --exclude '.fuse*'"
    fi

    localdir="$localdirbase/$subdir"
    destmounts_dir='/media/braup/'
    dests=$(ls $destmounts_dir)
    for d in $dests ; do
        size=$(df -h --output=size $destmounts_dir/$d | grep -v Size | tr -d ' ')
        if [ "$size" != "$req_size" ] ; then
            echo "This USB drive is the wrong size ($size). Skipping"
            continue
        fi
        dest="$destmounts_dir/$d/$subdir"
        mkdir -p $dest
        echo "Syncing $localdir to $dest"
        rsync --recursive --update --verbose --modify-window=4 \
              --delete \
              $excl                                                     \
              "$localdir/" "$dest/" &
    done
}


if [ "$1" = "" ] ; then
    echo "$usage"
    exit 1
fi

./mount_all.sh

case "$1" in
    "large")
        echo "Copying entire FTP site to USB drives. Must be the 64 GB ones."
        do_sync "." "59G"
        ;;
    "small")
        echo "Copying GIS_training and non-data materials for main training to USB drives. Must be the 16 GB ones."
        #do_sync "GIS_training" "15G"
        do_sync "main_training/modules" "15G"
        ;;
    *)
        echo "$usage"
        exit 1
esac

#./umount_all.sh
